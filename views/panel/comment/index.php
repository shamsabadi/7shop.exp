<div class="page-wrapper ssWrap ss-dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>کامنت های محصولات</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>کاربر</th>
                                    <th>محصول</th>
                                    <th>امتیاز از 10</th>
                                    <th>کامنت</th>
                                    <th>تاریخ ارسال</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($reviews as $review): ?>
                                    <tr>
                                        <td><?= $review->user->fullname ?></td>
                                        <td><?= $review->product->title ?></td>
                                        <td><?= $review->rate ?></td>
                                        <td><?= $review->comment ?></td>
                                        <td><?= $review->created_at ?></td>
                                        <td>
                                            <a href="<?= siteUrl('panel/comments/delete?comment=' . $review->id) ?>"><i class="fa fa-close"></i></a>
                                            <a href="<?= siteUrl('panel/comments/edit?comment=' . $review->id) ?>"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
