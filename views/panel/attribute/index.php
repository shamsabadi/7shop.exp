<div class="page-wrapper ssWrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card p-30">
                    <form action="<?php echo siteUrl('panel/attributes/create'); ?>" method="post">
                        <?= csrf_field(); ?>
                        <div class="form-group">
                            <label>نامک</label>
                            <input type="text" name="slug" class="form-control" value="" placeholder="مثلا : color">
                        </div>
                        <div class="form-group">
                            <label>عنوان فارسی </label>
                            <input type="text" name="title" class="form-control" value="" placeholder="مثلا : رنگ">
                        </div>
                        <div class="form-group">
                            <label>نوع مقدار</label>
                            <select name="type" class="form-control">
                                <option value="integer">عدد صحیح</option>
                                <option value="float">عدد اعشاری</option>
                                <option value="tinytext">رشته، متن</option>
                                <option value="bigtext">متن بزرگ</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info btn-flat btn-addon m-b-10">افزدن</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>عنوان نمایش </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>نامک</th>
                                    <th>عنوان</th>
                                    <th>نوع</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($attrs as $attr): ?>
                                    <tr>
                                        <td><?= $attr->id ?></td>
                                        <td><?= $attr->slug ?></td>
                                        <td><?= $attr->title ?></td>
                                        <td><?= $attr->type ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
