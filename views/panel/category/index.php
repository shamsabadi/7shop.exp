<div class="page-wrapper ssWrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card p-30">
                    <form action="<?php echo siteUrl('panel/categories/create'); ?>" method="post">
                        <?= csrf_field(); ?>
                        <div class="form-group">
                            <label>نامک</label>
                            <input type="text" name="slug" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label>عنوان فارسی </label>
                            <input type="text" name="title" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label>دسته بندی پدر</label>
                            <select name="parent" class="form-control">
                                <option value="0">دسته بندی اصلی</option>
                                <?php foreach ($cats as $cg): ?>
                                <option value="<?= $cg->id ?>"><?= $cg->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info btn-flat btn-addon m-b-10">افزدن</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>عنوان نمایش </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>نامک</th>
                                    <th>عنوان</th>
                                    <th>دسته بندی پدر</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($cats as $cat): ?>
                                    <tr>
                                        <td><?= $cat->id ?></td>
                                        <td><?= $cat->slug ?></td>
                                        <td><?= $cat->title ?></td>
                                        <td><?= $cat->parent ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
