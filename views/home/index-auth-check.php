<html>
<head>

    <link href="<?= asset('css', 'lib/bootstrap/bootstrap-rtl.min.css') ?>" rel="stylesheet">
    <link href="<?= asset('css', 'helper.css') ?>" rel="stylesheet">
    <link href="<?= asset('css', 'fonts.css') ?>" rel="stylesheet">
    <link href="<?= asset('css', 'style.css') ?>" rel="stylesheet">
    <style>
        body, html {
            height: 100%;
            background-repeat: no-repeat;
            background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));
        }

        .container.logincontainer {
            padding-top: 100px;
        }
    </style>
</head>
<body>
<div class="container logincontainer">
    <div class="card card-container" style="max-width: 700px">
        <div class="text-center"><a href="<?= siteUrl('auth/register') ?>">عضویت</a> :
            <a href="<?= siteUrl('auth/login') ?>">ورود</a> :
            <a href="<?= siteUrl('auth/logout') ?>">خروج</a>
        </div><br><br>
        <pre style="direction: ltr;text-align: left">
        User Exists ? <?= \App\Services\Auth\Auth::userExists(\App\Services\Auth\Auth::current_user_email()); ?><br><br>
        User Logged in ? <?= \App\Services\Auth\Auth::user_logged_in(); ?><br><br>
        Current Email ? <?= \App\Services\Auth\Auth::current_user_email(); ?><br><br>
        Current User ? <?php  var_dump(\App\Services\Auth\Auth::current_user()); ?><br><br>
        </pre>
    </div><!-- /card-container -->
</div><!-- /container -->
<?php \App\Services\Flash\FlashMessage::show_messages(); ?>

<!--Custom JavaScript -->
<script src="<?= asset('js', 'lib/jquery/jquery.min.js') ?>"></script>
<script src="<?= asset('js', 'custom.min.js') ?>"></script>
</body>
</html>