<html>
<head>

    <link href="<?= asset('css', 'lib/bootstrap/bootstrap-rtl.min.css') ?>" rel="stylesheet">
    <link href="<?= asset('css', 'helper.css') ?>" rel="stylesheet">
    <link href="<?= asset('css', 'fonts.css') ?>" rel="stylesheet">
    <link href="<?= asset('css', 'style.css') ?>" rel="stylesheet">
    <style>
        body, html {
            height: 100%;
            background-repeat: no-repeat;
            background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));
        }

        .container.logincontainer {
            padding-top: 100px;
        }
    </style>
</head>
<body>
<div class="container logincontainer">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="<?= asset('img', 'register.png') ?>"/>
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" method="post" action="<?= siteUrl('auth/doregister') ?>">
            <?= csrf_field(); ?>
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" id="inputEmail" name="fullname" class="form-control text-center" placeholder="نام کامل شما" required autofocus>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="ایمیل شما" required autofocus>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="رمز عبور" required>
            <input type="password" id="inputPassword" name="password2" class="form-control" placeholder="تکرار رمز عبور" required>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">ثبت نام</button>
        </form><!-- /form -->
        <div class="text-center"><a href="<?= siteUrl('auth/login')?>">ورود به سایت</a></div>
        <div class="text-center"><a href="<?= siteUrl() ?>">بازگشت به صفحه اصلی</a></div>
    </div><!-- /card-container -->
</div><!-- /container -->
<?php \App\Services\Flash\FlashMessage::show_messages(); ?>
<!--Custom JavaScript -->
<script src="<?= asset('js', 'lib/jquery/jquery.min.js') ?>"></script>
<script src="<?= asset('js', 'custom.min.js') ?>"></script>
</body>
</html>