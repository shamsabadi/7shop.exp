<?php

namespace App\Core;

use App\Services\Flash\FlashMessage;
use App\Services\Validator\Validator;

class Request
{
    public $method;
    public $uri;
    private $params;
    private $ip;
    private $agent;

    public function __construct()
    {
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->params = $_REQUEST;
    }

    public function param($key)
    {
        return $this->params[$key];
    }

    public function params()
    {
        return $this->params;
    }

    public function only($keys)
    {
        if (empty($keys)) {
            return [];
        }

        $only = array_filter($this->params, function ($k) use ($keys) {
            if (in_array($k, $keys)) {
                return true;
            }
        }, ARRAY_FILTER_USE_KEY);

        return $only;
    }
    public function except($keys = [])
    {
        if (empty($keys)) {
            return $this->params;
        }

        $arr = array_filter($this->params, function ($k) use ($keys) {
            if (!in_array($k, $keys)) {
                return true;
            }
        }, ARRAY_FILTER_USE_KEY);

        return $arr;
    }



    public function __get($name)
    {
        if (array_key_exists($name, $this->params)) {
            return $this->param($name);
        }
    }

    public function keyExists($key)
    {
        return isset($this->params[$key]);
    }

    public function removeParam($key)
    {
        unset($this->params[$key]);
    }
    public function isPost()
    {
        return $this->method == 'POST';
    }



    public function validate(array $validation_rules)
    {
        $vRes = Validator::is_valid($this->params, $validation_rules);
        if ($vRes === true) {
            return true;
        } else {
            foreach ($vRes as $errMsg) {
                FlashMessage::add($errMsg, FlashMessage::ERROR);
            }
        }
    }
    public function xss_clean()
    {
        $this->params = Validator::xss_clean($this->params);
    }
    public function sanitize($whitelist = null)
    {
        $validator = Validator::get_instance();
        $this->params = $validator->sanitize($this->params, $whitelist);
    }

    public static function redirect($route = '')
    {
        header("Location: " . siteUrl($route));
    }
}
