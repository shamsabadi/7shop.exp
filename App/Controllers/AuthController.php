<?php

namespace App\Controllers;

use App\Core\Request;
use App\Repositories\UserRepo;
use App\Services\Auth\Auth;
use App\Services\Flash\FlashMessage;
use App\Services\View\View;

class AuthController
{

    public function login(Request $request)
    {
        View::load('auth.login');
    }

    public function dologin(Request $request)
    {
        if (Auth::login($request->param('email'), $request->param('password'))) {
            FlashMessage::add("با موفقیت لاگین شدید", FlashMessage::SUCCESS);
            Request::redirect();
        } else {
            FlashMessage::add("نام کاربری یا رمز عبور وارد شده اشتباه است.", FlashMessage::ERROR);
            Request::redirect('auth/login');
        }
    }

    public function logout(Request $request)
    {
        if(Auth::user_logged_in()){
            Auth::logout();
            FlashMessage::add("از سایت خارج شدید", FlashMessage::INFO);
        }else{
            FlashMessage::add("شما در سایت لاگین نبوده اید", FlashMessage::INFO);
        }

        Request::redirect();
    }

    public function register(Request $request)
    {
        View::load('auth.register');
    }

    public function doregister(Request $request)
    {
        // password repeat check
        if ($request->param('password') != $request->param('password2')) {
            FlashMessage::add('رمز عبور و تکرار آن باید یکسان باشند.',FlashMessage::ERROR);
        }
        // input validation & filtering here
        $vRes = true && !FlashMessage::$has_error;   // result of validation and filtering
        if ($vRes == true) {
            $result = Auth::create_user($request->except(['csrf', 'password2']));
            if($result){
                FlashMessage::add("ثبت نام شما انجام شد", FlashMessage::SUCCESS);
                Request::redirect();
            }else{
                FlashMessage::add("ثبت نام شما انجام نشد. کاربری با این ایمیل قبلا در سایت ثبت نام کرده است.", FlashMessage::WARNING);
                Request::redirect('auth/register');
            }
        }else{
            Request::redirect('auth/register');
        }
    }



}