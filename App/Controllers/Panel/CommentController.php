<?php
namespace App\Controllers\Panel;

use App\Repositories\ReviewRepo;
use App\Services\View\View;

class CommentController{

	public function index($request) {
        $repo = new ReviewRepo();
        $data = [
            'reviews' => $repo->all()
        ];
		View::load('panel.comment.index',$data,'panel-admin');
	}
	
}