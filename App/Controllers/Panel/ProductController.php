<?php
namespace App\Controllers\Panel;

use App\Core\Request;
use App\Repositories\AttributeRepo;
use App\Repositories\CategoryRepo;
use App\Repositories\ProductRepo;
use App\Services\Flash\FlashMessage;
use App\Services\View\View;
use Illuminate\Support\Facades\Redirect;

class ProductController{

	public function all($request) {

        $repo = new ProductRepo();
        $data = [
            'products' => $repo->all(['category'])
        ];

		View::load('panel.product.all',$data,'panel-admin');
	}
	
	public function add($request) {
        $catRepo = new CategoryRepo();
        $attrRepo = new AttributeRepo();
        $data = [
            'cats' => $catRepo->all(),
            'attrs' => []
        ];
        if (isset($_GET['cat']) and is_numeric($_GET['cat'])) {
            $data['attrs'] = $catRepo->getAttributes($_GET['cat']);
        }
        View::load('panel.product.add',$data,'panel-admin');
	}
	public function save($request) {
        // input validation & filtering here
        $vRes = true;   // result of validation and filtering
        if ($vRes === true) {
            $productRepo = new ProductRepo();
            $product = $productRepo->create($request->only(['category_id', 'title', 'description', 'price', 'quantity']));
            $productRepo->setAttributes($product,$request->param('attr'));
            FlashMessage::add("انجام شد", FlashMessage::SUCCESS);
        }
        Request::redirect('panel/product/add');
	}
	
}