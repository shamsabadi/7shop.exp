<?php
namespace App\Controllers\Panel;

use App\Core\Request;
use App\Core\UploadedFile;
use App\Repositories\MediaRepo;
use App\Repositories\ProductRepo;
use App\Services\Flash\FlashMessage;
use App\Services\View\View;
class MediaController{

	public function index($request) {
		$data = [];
		View::load('panel.media.all',$data,'panel-admin');
	}
	
	public function add($request) {
	    $productRepo = new ProductRepo();
		$data = [
		    'products' => $productRepo->latest(20)
        ];

        View::load('panel.media.add',$data,'panel-admin');
    }

    public function save(Request $request)
    {
        // validation ...
        $file = new UploadedFile('media');
        $uploadedFileURL = $file->save();
        if($uploadedFileURL){
            $repo = new MediaRepo();
            $repo->create([
                'product_id' => $request->param('product_id') ,
                'type' => $request->param('type'),
                'url' => $uploadedFileURL
            ]);
            FlashMessage::add("فایل با موفقیت اضافه شد", FlashMessage::SUCCESS);
        }else{
            FlashMessage::add("مشکلی در هنگام آپلود فایل رخ داد", FlashMessage::ERROR);
        }
        Request::redirect('panel/media/add');
    }

}