<?php
namespace App\Utility;

class Cart
{
    const KEY = 'cart';
    const EXPIRE_TIME = 7 * 24 * 3600;

    public static function add($item, $count)
    {
        $cart = self::items();
        $cart[$item] = ($cart[$item] ?? 0) + $count  ;
        Cart::save($cart);
    }
    
    public static function remove($item)
    {
        if (!empty($_COOKIE[self::KEY])) { // no item in cart
            $cart = self::items();
            unset($cart[$item]);
            Cart::save($cart);
        }
    }
    public static function clear()
    {
        unset($_COOKIE[self::KEY]);
        setcookie(self::KEY, '', time() - 3600);
    }
    
    private static function save($cart)
    {
        setcookie(self::KEY, serialize($cart), time() + self::EXPIRE_TIME);
    }
    public static function items()
    {
        return (empty($_COOKIE[self::KEY])) ? array() : unserialize($_COOKIE[self::KEY]);
    }
    public static function itemIds()
    {
        return array_keys(self::items());
    }
    public static function size()
    {
        return sizeof(self::items());
    }
}
