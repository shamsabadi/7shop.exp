<?php
namespace App\Repositories;
use App\Models\Category;

class CategoryRepo extends BaseRepo {
    protected $model = Category::class;

    public function setAttributes($cat_id, $attrs)
    {
        $cat = $this->find($cat_id);
        if ($cat) {
            $cat->attributes()->sync($attrs);
        }
    }

    public function getAttributes($cat_id)
    {
        $cat = $this->find($cat_id);
        if ($cat) {
            return $cat->attributes()->get();
        }
        return [];
    }

    public function getAttributeIds($cat_id)
    {
        $cat = $this->find($cat_id);
        if ($cat) {
            return $cat->attributes()->pluck('attribute_id')->toArray();
        }
        return [];
    }


}